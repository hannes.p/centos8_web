#!/bin/sh

cd /etc/yum.repos.d/
wget http://yum.oracle.com/public-yum-ol7.repo
wget http://yum.oracle.com/RPM-GPG-KEY-oracle-ol7
rpm --import RPM-GPG-KEY-oracle-ol7

yum-config-manager --enable ol7_oracle_instantclient

#yum -y install oracle-instantclient19.3-devel

dnf -y install libnsl

#echo "instantclient,/usr/lib/oracle/19.3/client64/lib" | pecl install oci8

#yum-config-manager --disable ol7_oracle_instantclient
#yum-config-manager --disable ol7_UEKR5
#yum-config-manager --disable ol7_latest